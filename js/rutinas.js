
$(function(){  $("[data-toggle='tooltip']").tooltip(); });
$(function(){  $("[data-toggle='popover']").popover(); });

// Instruccion para controlar el tiempo en el CAROUSEL mediante una estructura json en segundos - 1000 as 1 segundo o 1 milisegundo
$('.carousel').carousel({
    interval: 50000
});

// Control de uso boton modal:
// 1º Sigue los eventos en consola
// 2º Cambia las propiedades sobre la apertura
// 3º Al cierre del modal regresa a su estado inicial

$('#contacto').on('show.bs.modal', function(e){
  console.log("Apertura de modal");
  $('#contactoBtn').removeClass('btn-outline-success');
  $('#contactoBtn').addClass('btnPrimary');
  $('#contactoBtn').prop('disabled',true);
});

$('#contacto').on('shown.bs.modal', function(e){
  console.log("Modal abierto");
});

$('#contacto').on('hide.bs.modal', function(e){
  console.log("Cierre de modal");
});

$('#contacto').on('hidden.bs.modal', function(e){
  console.log("Modal cerrado");
  $('#contactoBtn').addClass('btn-outline-success');
  $('#contactoBtn').removeClass('btnPrimary');
  $('#contactoBtn').prop('disabled',false);
});

